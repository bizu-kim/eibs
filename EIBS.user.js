// ==UserScript==
// @name         Enhances InnerBeans Script
// @namespace    https://bizu.tk/
// @description  try to take over the world!
// @version      0.4.2
// @require      http://code.jquery.com/jquery-1.12.4.min.js
// @downloadURL  https://bitbucket.org/bizu-kim/EIBS/raw/master/EIBS.user.js
// @updateURL    https://bitbucket.org/bizu-kim/EIBS/raw/master/EIBS.user.js
// @author       Bizu
// @match        https://login.innerbeans.info/*
// @require      https://raw.githubusercontent.com/ejci/favico.js/master/favico.js
// @grant        notification
// ==/UserScript==

(function () {
    'use strict';

    /////// favicon count

    var badge = 0;
    var favicon = new Favico({
        animation: 'popFade'
    });

    // Set the name of the hidden property and the change event for visibility
    var hidden, visibilityChange;
    if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
        hidden = "hidden";
        visibilityChange = "visibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
        hidden = "webkitHidden";
        visibilityChange = "webkitvisibilitychange";
    }

    function handleVisibilityChange() {
        if (!document[hidden]) {
            badge = 0;
            favicon.reset();
        }
    }

    // Warn if the browser doesn't support addEventListener or the Page Visibility API
    if (typeof document.addEventListener === "undefined" || hidden === undefined) {
        console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
    } else {
        // Handle page visibility change   
        document.addEventListener(visibilityChange, handleVisibilityChange, false);
    }

    function addCount() {
        if (document[hidden]) {
            badge = badge + 1;
            favicon.badge(badge);
        }
    }

    /////// favicon count end


    /////// link preview 

    // var BING_ENDPOINT = "https://api.labs.cognitive.microsoft.com/urlpreview/v7.0/search";
    // var key = "2b2b79e92d4947df8bc4370f9b61bbc2";
    // var xhr;
    // var data;


    // $('.scroll-comment-wrapper')[0].addEventListener("focus", function (e) {
    //     $('.url-link').addEventListener("focus", getPreview);
    // });

    // function getPreview() {
    //     console.log(data);
    //     xhr = new XMLHttpRequest();
    //     data = $(".url-link");
    //     console.log(data);
    //     data = data[data.length - 1];
    //     console.log(data);

    //     var queryUrl = BING_ENDPOINT + "?mkt=ko-KR&q=" + encodeURIComponent(data.href);
    //     xhr.open('GET', queryUrl, true);
    //     xhr.setRequestHeader("Ocp-Apim-Subscription-Key", key);

    //     xhr.send();

    //     xhr.addEventListener("readystatechange", processRequest, false);
    // }

    // function processRequest(e) {
    //     if (xhr.readyState == 4 && xhr.status == 200) {
    //         var obj = JSON.parse(xhr.responseText);
    //         var desc = document.createElement("div");
    //         desc.setAttribute("class", "description")
    //         desc.innerHTML = obj.description;
    //         var img = document.createElement("img");
    //         img.setAttribute("class", "link-preview");
    //         img.width = 350;
    //         img.src = obj.primaryImageOfPage.contentUrl;
    //         var div = document.createElement("div");
    //         div.appendChild(img);
    //         div.appendChild(desc);
    //         data.appendChild(div);
    //     }
    // }

    /////// link preview end


    /////// notification 

    function showNotiFn(data) {
        var title = data.summary;
        var notiData = {
            body: $("<div></div>").html(data.message).text(),
            icon: data.fromUserAvatarImageUrl,
            requireInteraction: false
        };
        var notification = new Notification(title, notiData, data);
        speechSynthesis.cancel();
        speechSynthesis.speak(new SpeechSynthesisUtterance(notiData.body.substr(0,200)));
        var clientWindow = window;
        notification.onclick = function (event) {
            event.preventDefault();
            /* prevent the browser from focusing the Notification's tab*/
            // var url = location.origin + "/" + data.projectId + "/collaboration/" + data.issueId, '_blank;
            clientWindow.focus();
            app.global.router.goToCollaborationDetail(data.issueId);
        };
        setTimeout(function () {
            notification.close();
        }, 10000);
    }

    var originalfunction = app.global.alarm.showNotification;
    app.global.alarm.showNotification = function (data){
        addCount();
        showNotiFn(data);
        originalfunction(data);
    }

    /////// notification end
})();